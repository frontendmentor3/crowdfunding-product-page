# Frontend Mentor - Crowdfunding product page solution

This is a solution to
the [Crowdfunding product page challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/crowdfunding-product-page-7uvcZe7ZR)
. Frontend Mentor challenges help you improve your coding skills by building realistic projects.

## Table of contents

- [Frontend Mentor - Crowdfunding product page solution](#frontend-mentor---crowdfunding-product-page-solution)
    - [Table of contents](#table-of-contents)
    - [Overview](#overview)
        - [The challenge](#the-challenge)
        - [Screenshot](#screenshot)
        - [Links](#links)
    - [My process](#my-process)
        - [Built with](#built-with)
        - [What I learned](#what-i-learned)
        - [Continued development](#continued-development)
        - [Useful resources](#useful-resources)
    - [Author](#author)

## Overview

### The challenge

Users should be able to:

- View the optimal layout depending on their device's screen size
- See hover states for interactive elements
- Make a selection of which pledge to make
- See an updated progress bar and total money raised based on their pledge total after confirming a pledge
- See the number of total backers increment by one after confirming a pledge
- Toggle whether or not the product is bookmarked

### Screenshot

[![](https://i.imgur.com/eD18XFPm.jpg)](https://i.imgur.com/eD18XFP.png)
[![](https://i.imgur.com/XuTnBRlm.jpg)](https://i.imgur.com/XuTnBRl.png)
[![](https://i.imgur.com/UKSX08Gm.jpg)](https://i.imgur.com/UKSX08G.png)
[![](https://i.imgur.com/S1CswaIm.jpg)](https://i.imgur.com/S1CswaI.png)
[![](https://i.imgur.com/pbIZ1lDm.jpg)](https://i.imgur.com/pbIZ1lD.png)

### Links

- Solution URL: [Gitlab](https://gitlab.com/frontendmentor3/crowdfunding-product-page)
- Live Site URL: [Netlify](https://frontendmentor-crowdfunding-gitlab.netlify.app/)

## My process

### Built with

- Flexbox
- CSS Grid
- Mobile-first workflow
- [React](https://reactjs.org/) - JS library
- [Styled Components](https://styled-components.com/) - For styles

### What I learned

- using modal with React
- CSS filter

#### <u>close modal when clicking outside of it</u>

modal is structured like this, in public/index.html

```html

<div id="root"></div>
<div id="modal-root"></div>
```

`App.js`

```js 
<div ref={rootRef} onClick={handleClick}>
    <Modal
        open={isModalOpen}
        onModalClose={() => {
            closeModal();
        }}
    />
    <App>
        <SummaryCard onModalOpen={openModal}/>
        <StatsCard/>
        <AboutCard/>
    </App>
</div>    
```

The modal was put into ```#modal-root``` using react portal in `src/components/Modal/Modal.js`

To detect when the user clicks outside of the modal, a ref was used to see if the user clicked on the `#root` div.
Modal will only close is it is currently opened.

```js
const handleClick = (e) => {
    if (rootRef.current.contains(e.target) && isModalOpen) {
        closeModal();
    }
};

```

To disable scrolling, and adjust opacity on root div when the modal is opened,
opacity change on open is in ```src/components/Modal/Modal.js```

```js
const openModal = () => {
    setIsModalOpen(true);
    document.body.style.overflow = 'hidden';
};

const closeModal = () => {
    setIsModalOpen(false);
    document.getElementById('root').style.filter = 'opacity(100%)';
    document.body.style.overflow = 'unset';
};
```

Hash Links do not work with 'react-router-dom',
use [React Router Hash Link](https://www.npmjs.com/package/react-router-hash-link)

### Continued development

possible improvements:
[ ] use context to deal with closing and opening of the modal since it's passed down too many levels via props
[ ] darken background when menu is opened in mobile layout

### Useful resources

- [How to render modals in React - ReactDOM Portal](https://www.freecodecamp.org/news/how-to-render-modals-in-react-bbe9685e947e/)

- [Learn React Portal In 12 Minutes By Building A Modal](https://www.youtube.com/watch?v=LyLa7dU5tp8)

## Author

- Website - [Cheryl M](cherylm.dev)
- Github - [cherylli](https://github.com/cherylli)
- Gitlab - [cherylm](https://gitlab.com/cherylm)
- Frontend Mentor - [@cherylli](https://www.frontendmentor.io/profile/cherylli)



