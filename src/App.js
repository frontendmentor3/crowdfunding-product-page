import {useState, createRef} from 'react';
import {BrowserRouter as Router} from 'react-router-dom';
import styled from 'styled-components';
import AboutCard from './components/AboutCard';
import Modal from './components/Modal/Modal';
import StatsCard from './components/StatsCard';
import SummaryCard from './components/SummaryCard';
import desktopBackground from './images/image-hero-mobile.jpg';
import Navigation from './components/navigation/Navigation';


const AppStyles = styled.div`
  background-image: url(${desktopBackground});
  background-size: 100% auto;
  background-repeat: no-repeat;

`;

const AppInnerContainerStyles = styled.div`
  max-width: 800px;
  min-width: 300px;
  margin: 0 auto;
  display: flex;
  flex-direction: column;
  gap: 20px;
  padding-top: 200px;
`;

const App = () => {
    const [isModalOpen, setIsModalOpen] = useState(false);
    const rootRef = createRef();

    const openModal = () => {
        setIsModalOpen(true);
        document.body.style.overflow = 'hidden';
    };

    const closeModal = () => {
        setIsModalOpen(false);
        document.getElementById('root').style.filter = 'opacity(100%)';
        document.body.style.overflow = 'unset';
    };

    const handleClick = (e) => {
        if (rootRef.current.contains(e.target) && isModalOpen) {
            closeModal();
        }
    };

    return (
        <div ref={rootRef} onClick={handleClick}>
            <Router>
                <Modal
                    open={isModalOpen}
                    onModalClose={() => {
                        closeModal();
                    }}
                />
                <AppStyles>
                    <AppInnerContainerStyles>
                        <Navigation/>
                        <SummaryCard onModalOpen={openModal}/>
                        <StatsCard/>
                        <AboutCard/>
                    </AppInnerContainerStyles>
                </AppStyles>
            </Router>
        </div>
    );
};

export default App;
