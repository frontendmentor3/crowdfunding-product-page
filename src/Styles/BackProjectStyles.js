import styled from 'styled-components';
import baseCardStyles from './CardStyles';

export const BackProjectStyles = styled(baseCardStyles)`
  color: var(--dark-gray);
  margin-bottom: 40px;
  padding-bottom: 40px;
  min-width: 300px;

  & > h1 {
    align-self: flex-start;
    padding-top: 20px;
    font-size: 1.1rem;
    color: black;
  }

  & > div {
    display: flex;
    flex-direction: column;
    gap: 20px;
  }
`;

export const ProductCardStyles = styled.div`
  border: ${props => props.isSelected ?
          "2px solid var(--primary)" :
          "2px solid var(--bg-color)"};
  border-radius: 10px;
  padding: 0px 20px 20px 20px;

  .card-header {
    margin-top: 20px;
    display: flex;
    align-self: flex-start;
  }

  input[type='radio'] {
    margin-right: 10px;
    width: 1rem;
    height: 1rem;
    align-self: center;
  }

  input[type='radio']:before {
    content: '';
    background-clip: content-box;
  }

  input[type='radio']:after {
    background-color: var(--primary);
  }

  input[type='radio']:checked:after {
    background-color: var(--primary);
  }

  .title {
    font-size: 1rem;
    text-align: left;
    color: black;
    margin: 0px;
  }

  .subtitle {
    color: var(--primary);
    font-weight: bold;
  }

  .inner-card-header {
    display: flex;
    flex-direction: column;
  }

  & > p {
    line-height: 1.6rem;
    font-size: 0.9rem;
  }

  & > div > span {
    font-size: 2rem;
    font-weight: bold;
    color: black;
    margin: 0 10px 10px 0;
  }

  & > button {
    margin-top: 20px;
    border: 0px;
    background-color: var(--primary);
    color: white;
    padding: 0.8rem 2rem;
    border-radius: 9999em;
    font-weight: 500;
  }

  &.out-of-stock {
    filter: opacity(50%);

    & > button {
      background-color: var(--dark-gray);
    }

    input[type='radio'] {
      pointer-events: none;
    }
  }
`;

export const EnterPledgeStyles = styled.div`
  border-top: 2px solid var(--bg-color);
  margin-top: 20px;
  padding: 30px 10px 10px 10px;
  display: flex;
  flex-direction: column;

  .buttons-container {
    display: flex;
    gap: 10px;
    justify-content: space-between;
  }


  .title {
    color: var(--dark-gray);
    padding-bottom: 20px;
    text-align: center;
  }

  .pledge-amount-wrapper, .continue-button {
    padding: 15px 35px;
    border-radius: 40px;
  }

  .pledge-amount-wrapper {
    border: 1px solid var(--dark-gray);
    display: flex;
    align-items: center;
  }


  input {
    width: 40px;
    border: 0px;
    font-size: 1.1rem;
    font-weight: bold;
    -webkit-appearance: none;
    -moz-appearance: textfield;
  }

  input:focus {
    outline: none;
  }

  .continue-button {
    background-color: var(--primary);
    color: white;
    line-height: 25px;
    cursor: pointer;
  }

  .continue-button:hover {
    background-color: var(--dark-primary);
  }
`;