import styled from 'styled-components';

const CardStyles = styled.div`
  border: 0px;
  border-radius: 10px;
  padding: 0rem 1rem;
  margin: 0px 10px;
  background-color: white;
  display: flex;
  flex-direction: column;
  align-items: center;
  max-width: 1000px;
  justify-content: space-around;
`;

export default CardStyles;
