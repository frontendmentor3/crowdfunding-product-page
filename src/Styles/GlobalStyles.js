import {createGlobalStyle} from 'styled-components';
import {fontSize} from './Sizes';

export const GlobalStyle = createGlobalStyle`
  :root {
    --primary: hsl(176, 50%, 47%); //#3cb4ac
    --dark-primary: hsl(176, 72%, 28%); //#147b74
    --black: hsl(0, 0%, 0%);
    --white: hsl(0, 0%, 100%);
    --dark-gray: hsl(0, 0%, 48%);
    --bg-color: hsla(0, 0%, 91%, 1);
    --menu-background: rgba(122, 122, 122, 0.6);
  }

  html {
    font-size: ${fontSize.primary};
    font-family: 'Commissioner', sans-serif;
  }

  body {
    padding: 0;
    margin: 0;
    background-color: var(--bg-color);
  }

`;