export const fontSize = {
    primary: '16px',
};

// transition point from mobile to desktop layout
const SCREEN_SIZES = {
    desktop: '800px'
};

export const devices = {
    desktop: `(min-width:${SCREEN_SIZES.desktop})`
};
