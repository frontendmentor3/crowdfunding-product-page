import styled from 'styled-components';
import baseCardStyles from '../Styles/CardStyles';

const AboutStyles = styled(baseCardStyles)`
  color: var(--dark-gray);
  line-height: 1.5rem;
  margin-bottom: 40px;
  padding-bottom: 40px;

  h1 {
    align-self: flex-start;
    padding-top: 20px;
    font-size: 1.1rem;
    color: black;
  }

  & > div {
    display: flex;
    flex-direction: column;
    gap: 20px;
  }
`;

const ProductCardStyles = styled.div`
  border: 2px solid var(--bg-color);
  border-radius: 5px;
  padding: 0px 20px 20px 20px;

  & > h1 {
    font-size: 1rem;
  }

  .subtitle {
    color: var(--primary);
    font-weight: bold;
  }

  & > p {
    line-height: 1.6rem;
    font-size: 0.9rem;
  }

  & > div > span {
    font-size: 2rem;
    font-weight: bold;
    color: black;
    margin: 0 10px 10px 0;
  }

  & > button {
    margin-top: 20px;
    border: 0px;
    background-color: var(--primary);
    color: white;
    padding: 0.8rem 2rem;
    border-radius: 9999em;
    font-weight: 500;
    cursor: pointer;
  }

  & > button:hover {
    background-color: var(--dark-primary);
  }

  &.out-of-stock {
    filter: opacity(50%);

    & > button {
      background-color: var(--dark-gray);
    }
  }
`;

const ProductCard = (props) => {
    return (
        <ProductCardStyles quantity={props.quantity} className={props.className}>
            <h1>{props.title}</h1>
            <div className="subtitle">{props.subtitle}</div>
            <p>{props.desc}</p>
            <div>
                <span>{props.quantity}</span> left
            </div>
            <button>{props.quantity === 0 ? `Out of Stock` : `Select Reward`}</button>
        </ProductCardStyles>
    );
};

const AboutCard = () => {
    return (
        <AboutStyles id="about">
            <h1>About this project</h1>
            <p>
                The Mastercraft Bamboo Monitor Riser is a sturdy and stylish platform
                that elevates your screen to a more comfortable viewing height. Placing
                your monitor at eye level has the potential to improve your posture and
                make you more comfortable while at work, helping you stay focused on the
                task at hand.
                <br/>
                <br/>
                Featuring artisan craftsmanship, the simplicity of design creates extra
                desk space below your computer to allow notepads, pens, and USB sticks
                to be stored under the stand.
            </p>
            <div>
                <ProductCard
                    title="Bamboo Stand"
                    subtitle="Pledge $25 or more"
                    desc="You get an ergonomic stand made of natural bamboo. You've helped us launch our promotional campaign, and
  you’ll be added to a special Backer member list."
                    quantity={101}
                />
                <ProductCard
                    title="Black Edition Stand"
                    subtitle="Pledge $75 or more"
                    desc="You get a Black Special Edition computer stand and a personal thank you. You’ll be added to our Backer
        member list. Shipping is included."
                    quantity={64}
                />
                <ProductCard
                    className="out-of-stock"
                    title="Mahogany Special Edition"
                    subtitle="Pledge $200 or more"
                    desc="You get two Special Edition Mahogany stands, a Backer T-Shirt, and a personal thank you. You’ll be added
        to our Backer member list. Shipping is included."
                    quantity={0}
                />
            </div>
        </AboutStyles>
    );
};

export default AboutCard;

/*
  
  
  64 left
  Select Reward
  
  Pledge $200 or more
  
  0 left
  Out of Stock
  */
