import {useState} from 'react';
import {BackProjectStyles, ProductCardStyles, EnterPledgeStyles} from '../../Styles/BackProjectStyles';


const ProductCard = (props) => {
    const {
        quantity,
        className,
        title,
        value,
        defaultValue,
        onValueChange,
        onRadioChange,
        openConfirmation,
        desc,
        isSelected
    } = props;

    return (
        <ProductCardStyles isSelected={isSelected} className={className}>
            <div class="card-header">
                <input type="radio"
                       name="product-select-radio"
                       onClick={() => onRadioChange(title, defaultValue)}/>
                <div className="inner-card-header">
                    <h1 className="title">{title}</h1>
                    <div className="subtitle">{value === 0 ? '' : `Pledge $${defaultValue} or more`}</div>
                </div>
            </div>
            <p>{desc}</p>
            <div>
                <span>{quantity}</span> {quantity ? `left` : ``}
            </div>
            {isSelected && (
                <EnterPledgeStyles>
                    <div className="title">Enter your pledge</div>
                    <div className="buttons-container">
                        <div className="pledge-amount-wrapper">
                            <span>$</span>
                            <input type="number"
                                   onChange={onValueChange}
                                   value={value}/>
                        </div>
                        <div className="continue-button"
                             onClick={openConfirmation}>Continue
                        </div>
                    </div>
                </EnterPledgeStyles>
            )}
        </ProductCardStyles>
    );
};

const BackProject = ({onModalClose, openConfirmation}) => {
    const [selectedTitle, setSelectedTitle] = useState('');
    const [pledgeAmount, setPledgeAmount] = useState(0);

    const handleInputChange = (event) => {
        // only update the state if the input is a number
        const re = /^[0-9\b]+$/;
        if (event.target.value === 0 || re.test(event.target.value)) {
            setPledgeAmount(event.target.value);
        }
    };


    const handleRadioChange = (title, pledgeAmount) => {
        setSelectedTitle(title);
        setPledgeAmount(pledgeAmount);
    };
    return (
        <BackProjectStyles>
            <h1>Back this project</h1>
            <p>
                Want to support us in brining Mastercraft Bamboo Monitor Riser out in
                the world?
            </p>
            <div>
                <ProductCard
                    title="Pledge with no reward"
                    desc="Choose to support us without a reward if you simply believe in our project. As a backer, you will be signed up to receive product updates via email."
                    isSelected={selectedTitle === 'Pledge with no reward'}
                    defaultValue={0}
                    value={pledgeAmount}
                    onValueChange={handleInputChange}
                    onRadioChange={handleRadioChange}
                    onModalClose={onModalClose}
                    openConfirmation={openConfirmation}
                />
                <ProductCard
                    title="Bamboo Stand"
                    desc="You get an ergonomic stand made of natural bamboo. You've helped us launch our promotional campaign, and
  you’ll be added to a special Backer member list."
                    quantity={101}
                    isSelected={selectedTitle === 'Bamboo Stand'}
                    defaultValue={25}
                    value={pledgeAmount}
                    onValueChange={handleInputChange}
                    onRadioChange={handleRadioChange}
                    onModalClose={onModalClose}
                    openConfirmation={openConfirmation}
                />
                <ProductCard
                    title="Black Edition Stand"
                    isSelected={selectedTitle === 'Black Edition Stand'}
                    desc="You get a Black Special Edition computer stand and a personal thank you. You’ll be added to our Backer
        member list. Shipping is included."
                    quantity={64}
                    defaultValue={75}
                    value={pledgeAmount}
                    onValueChange={handleInputChange}
                    onRadioChange={handleRadioChange}
                    onModalClose={onModalClose}
                    openConfirmation={openConfirmation}
                />
                <ProductCard
                    className="out-of-stock"
                    title="Mahogany Special Edition"
                    isSelected={selectedTitle === 'Mahogany Special Edition'}
                    defaultValue={200}
                    desc="You get two Special Edition Mahogany stands, a Backer T-Shirt, and a personal thank you. You’ll be added
        to our Backer member list. Shipping is included."
                    quantity={0}
                />
            </div>
        </BackProjectStyles>
    );
};

export default BackProject;
