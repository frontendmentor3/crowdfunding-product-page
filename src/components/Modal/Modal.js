import React from 'react';
import ReactDOM from 'react-dom';
import styled from 'styled-components';
import BackProject from './BackProject';
import ThankYouConfimration from './ThankYouConfimration';

export const ModalStyles = styled.div`
  position: fixed;
  top: 50px;
  left: 50%;
  transform: translateX(-50%);
  padding: 0px;
  width: 90vw;
  height: 100%;
  z-index: 1000;
  overflow: auto;
`;

function Modal({open, onModalClose}) {
    const [confirmation, setConfirmation] = React.useState(false);

    if (!open) return null;
    document.getElementById('root').style.filter = 'opacity(50%)';
    return ReactDOM.createPortal(
        <ModalStyles>
            {confirmation ?
                <ThankYouConfimration onModalClose={onModalClose}/> :
                <BackProject onModalClose={onModalClose}
                             openConfirmation={() => setConfirmation(true)}/>
            }

        </ModalStyles>,
        document.querySelector('#modal-root')
    );
}

export default Modal;
