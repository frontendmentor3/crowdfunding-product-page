import styled from 'styled-components';
import baseCardStyles from '../../Styles/CardStyles';
import checkIcon from '../../images/icon-check.svg';


const ThankYouConfirmationStyles = styled(baseCardStyles)`
  img {
    margin: 40px 0 20px 0;
  }

  h1 {
    font-size: 1.5rem;
  }

  p {
    color: var(--dark-gray);
    text-align: center;
    line-height: 1.5;
  }

  .btn {
    background-color: var(--primary);
    color: white;
    padding: 0.8rem 2rem;
    border-radius: 9999em;
    margin-top: 20px;
    margin-bottom: 40px;
    cursor: pointer;
  }

  .btn:hover {
    background-color: var(--dark-primary);
  }
`;

const ThankYouConfirmation = ({onModalClose}) => {
    return (
        <ThankYouConfirmationStyles>
            <img alt="check-icon" src={checkIcon}/>
            <h1>
                Thanks for your Support!
            </h1>
            <p>
                Your pledge brings us one step closer to sharing Mastercraft
                Bamboo Monitor Riser worldwide. You will
                get an email once our campaign is completed.
            </p>
            <div className="btn" onClick={onModalClose}>Got it!</div>
        </ThankYouConfirmationStyles>
    );
};

export default ThankYouConfirmation;