import styled from 'styled-components';
import baseCardStyles from '../Styles/CardStyles';

const StatsCardStyles = styled(baseCardStyles)`
  text-align: center;

  & > div:first-child {
    padding-top: 40px;
  }

  hr {
    border: 1px solid var(--bg-color);
    width: 100px;
  }
`;

const StatStyles = styled.div`
  & > p {
    margin-top: -10px;
    color: var(--dark-gray);
  }
`;

const ProgressBarStyles = styled.div`
  height: 10px;
  width: 100%;
  padding: 1px;
  margin-bottom: 40px;
  background-color: var(--bg-color);
  border-radius: 9999em;

  & > span {
    background-color: var(--primary);
    display: block;
    height: 100%;
    border-radius: 9999em;
  }
`;

// use props for the stats for more dynamic contents
const StatsCard = () => {
    return (
        <StatsCardStyles id="discover">
            <StatStyles>
                <h1>$89,914</h1>
                <p>of $100,000 backed</p>
            </StatStyles>
            <hr/>
            <StatStyles>
                <h1>5,007</h1>
                <p>total backers</p>
            </StatStyles>
            <hr/>
            <StatStyles>
                <h1>56</h1>
                <p>days left</p>
            </StatStyles>
            <ProgressBarStyles>
                <span style={{width: '80%'}}></span>
            </ProgressBarStyles>
        </StatsCardStyles>
    );
};

export default StatsCard;
