import {useState} from 'react';
import styled from 'styled-components';
import bookmarkIconTrue from '../images/icon-bookmark-true.svg';
import bookmarkIconFalse from '../images/icon-bookmark-false.svg';
import logoMasterCraft from '../images/logo-mastercraft.svg';
import baseCardStyles from '../Styles/CardStyles';

const SummaryStyles = styled(baseCardStyles)`
  & > img {
    position: relative;
    top: -30px;
  }

  h1 {
    font-weight: 700;
    font-size: 1.5rem;
    text-align: center;
  }

  p {
    text-align: center;
  }

  button {
    cursor: pointer;
    border: 0px;
    background-color: var(--primary);
    color: white;
    padding: 0.8rem 2rem;
    border-radius: 9999em;
    font-weight: 500;
    font-size: 1.1rem;
  }

  button:hover {
    background-color: var(--dark-primary);
  }

  .div-back-project {
    margin: 20px 0 30px 0;
    width: 100%;
    display: flex;
    justify-content: space-between;
  }

  .bookmark {
    cursor: pointer;
  }
`;

const SummaryCard = ({onModalOpen}) => {
    const [bookmarked, setBookmarked] = useState(false);
    return (
        <SummaryStyles id="get-started" bookmarked={bookmarked}>
            <img alt="master craft logo" src={logoMasterCraft}/>
            <h1>Mastercraft Bamboo Monitor Riser</h1>
            <p>
                A beautifully handcrafted monitor stand to reduce neck and eye strain.
            </p>
            <div className="div-back-project">
                <button onClick={onModalOpen}>Back this project</button>
                <img
                    alt="bookmark"
                    src={bookmarked === true ? bookmarkIconTrue : bookmarkIconFalse}
                    className="bookmark"
                    onClick={() => setBookmarked(!bookmarked)}
                />
            </div>
        </SummaryStyles>
    );
};

export default SummaryCard;
