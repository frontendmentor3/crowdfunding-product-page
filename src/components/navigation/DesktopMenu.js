import styled from 'styled-components';
import {HashLink} from 'react-router-hash-link';
import {devices} from '../../Styles/Sizes';

const DesktopMenuStyles = styled.ul`
  display: none;
  gap: 20px;
  margin-right: 40px;
  background-color: var(--menu-background);
  padding: 10px 20px;
  cursor: pointer;

  li {
    list-style: none;
  }

  a {
    text-decoration: none;
    color: var(--white);
  }

  @media ${devices.desktop} {
    display: flex;
    position: fixed;
    right: 0;
    top: 0;
  }

`;

const DesktopMenu = () => {
    return (
        <>

            <DesktopMenuStyles>
                <li><HashLink to="#about"><a href="#about">About</a></HashLink></li>
                <li><HashLink to="#get-started"><a href="#get-started">Discover</a></HashLink></li>
                <li><HashLink to="#discover"><a href="#discover">Get Started</a></HashLink></li>
            </DesktopMenuStyles>


        </>

    );
};

export default DesktopMenu;