import styled from 'styled-components';
import {HashLink} from 'react-router-hash-link';

const MobileMenuStyles = styled.ul`

  position: fixed;
  top: 50px;
  left: 10%;
  width: 80%;
  text-align: left;
  padding: 0;
  background-color: var(--white);
  border-radius: 10px;
  border: 1px solid var(--bg-color);
  box-shadow: 2px 2px 2px var(--bg-color);

  li {
    list-style: none;
    font-weight: bold;
    padding: 20px;
    margin: 10px 0;
  }

  li: not(: last-child) {
  border-bottom: 1px solid var(--bg-color);
}

  a {
    color: var(--black);
    text-decoration: none;
  }
`;

const MobileMenu = () => {
    return (
        <MobileMenuStyles>
            <li><HashLink to="#about"><a href="#about">About</a></HashLink></li>
            <li><HashLink to="#get-started"><a href="#get-started">Discover</a></HashLink></li>
            <li><HashLink to="#discover"><a href="#discover">Get Started</a></HashLink></li>
        </MobileMenuStyles>
    );
};

export default MobileMenu;