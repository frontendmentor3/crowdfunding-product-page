import styled from 'styled-components';
import logo from '../../images/logo.svg';
import hamburgerIcon from '../../images/icon-hamburger.svg';
import {useState} from 'react';
import {devices} from '../../Styles/Sizes';
import MobileMenu from './MobileMenu';
import DesktopMenu from './DesktopMenu';

const NavigationStyles = styled.nav`
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
  z-index: 100;


  img {
    padding: 20px;
  }

  .menu-icon {
    cursor: pointer;
    position: fixed;
    right: 0;
    top: 0;
    background-color: var(--menu-background);
  }

  @media ${devices.desktop} {
    .menu-icon {
      display: none
    }
  }
`;

const Navigation = () => {
    const [isMenuOpen, setIsMenuOpen] = useState(false);

    return (
        <NavigationStyles>
            <img src={logo} alt="logo"/>
            {isMenuOpen && <MobileMenu/>}
            <DesktopMenu/>
            <img className="menu-icon" src={hamburgerIcon} alt="menu-icon" onClick={() => setIsMenuOpen(!isMenuOpen)}/>
        </NavigationStyles>
    );
};

export default Navigation;